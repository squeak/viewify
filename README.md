# viewify

Display a database entry (json object) in a graphical way.

For the full documentation, installation instructions... check [viewify documentation page](https://squeak.eauchat.org/libs/viewify/).
