var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/color");
var $ = require("yquerj");
var uify = require("uify");
var windify = require("windify");
var views = require("./view");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CALCULATE OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function calculateOptions (options) {
  return $$.defaults(
    // default options
    { entry: {}, },
    // passed options
    options,
    // options to recalculate at each refresh
    _.isFunction(options.viewifyOptionsMaker) ? options.viewifyOptionsMaker() : undefined
  );
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              MAKE FULL OPTIONS

  options = calculateOptions(options);

  //
  //                              MAKE VIEWIFIED OBJECT

  var viewified = {
    isViewified: true,
    entry: options.entry,
    options: options,
    storage: options.storage || {},
    refresh: function (newEntry) {
      viewified.options = calculateOptions(options);
      if (newEntry) viewified.entry = newEntry;
      viewified.windified.refresh();
    },
  };

  //
  //                              DIALOG MENUBAR BUTTONS

  var menubarButtons = $$.array.merge(
    viewified.options.buttons || [],
    [{
      title: "display raw view",
      icomoon: "code",
      key: "shift + r",
      clickSelect: function (e) {
        viewified.tabs.open("raw")
      },
      clickUnselect: function (e) {
        viewified.tabs.open("normal")
      },
    }]
  );

  //
  //                              DISPLAY DIALOG

  viewified.windified = windify({
    $container: viewified.options.$container,
    title: viewified.options.title,
    subtitle: viewified.options.subtitle,
    bodyColor: viewified.options.bodyColor,
    titleBarColor: viewified.options.titleBarColor,
    icon: viewified.options.icon,
    image: viewified.options.image,
    windifyOptionsMaker: viewified.options.windifyOptionsMaker,
    css: viewified.options.cssWindow,
    class: "viewify",
    buttons: menubarButtons,
    buttonsContext: viewified,
    close: function () {
      var windified = this;
      var closeReturn = viewified.options.beforeClose ? viewified.options.beforeClose.call(viewified) : true;
      if (closeReturn !== false) {
        windified.destroy();
        if (viewified.options.closed) viewified.options.closed.call(viewified);
      };
    },
    contentMaker: function ($container) {
      var windified = this;

      // MAKE TABS
      viewified.tabs = uify.tabs({
        $container: $container,
        menuClass: "viewify-tabs_menu",
        tabs: _.map(views, function (viewFunc, viewName) {
          return {
            name: viewName,
            content: _.bind(viewFunc, viewified),
          };
        }),
      });

    },
  });

  //
  //                              ADD CLASS CATEGORY TO CREATED WINDOW

  if (viewified.options.category) viewified.windified.$window.addClass("viewify-"+ viewified.options.category);

  //
  //                              RETURN VIEWIFIED

  return viewified;

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
