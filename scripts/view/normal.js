var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var $ = require("yquerj");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILL FIELD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function fillField (viewified, $field, keyConfig, entry) {

  // KEY
  var $key = $field.span({
    class: "field-key "+ (keyConfig.labelLayout || "hidden"),
    htmlSanitized: keyConfig.label || keyConfig.deepKey,
  });
  if (keyConfig.keyCss) $key.css(keyConfig.keyCss);

  // VALUE
  var $fieldValue = $field.span({
    class: "field-value",
  });

  // show spinner if display is asynchronous
  if (displayIsAsync === "async") var spinner = uify.spinner({ $container: $fieldValue, });

  // add value from function display
  if (_.isFunction(keyConfig.display)) var displayIsAsync = keyConfig.display.call(viewified, entry, $fieldValue, $field, keyConfig.deepKey, function () {
    if (spinner) spinner.destroy();
    if ($fieldValue[0].innerHTML || keyConfig.showEvenIfUndefined) $field.show();
  })
  // add value from deepKey
  else {
    var htmlifiedStringifiedValue = $$.string.htmlify(
      $$.string.make(
        $$.getValue(entry, keyConfig.deepKey)
      )
    );
    $fieldValue.htmlSanitized(htmlifiedStringifiedValue);
  };

  // postprocess field
  if (_.isFunction(keyConfig.postprocess)) keyConfig.postprocess.call(viewified, $field, keyConfig);

  // hide field and key if no value (use innerHTML because can be image input for example that don't contain text)
  if (!$fieldValue[0].innerHTML && !keyConfig.showEvenIfUndefined) $field.hide();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function ($container) {
  var viewified = this;

  //
  //                              FILL FIELDS

  _.each(viewified.options.fields, function (fieldConfig) {

    // CREATE FIELD
    var $field = $container.div({ class: "field", });
    if (fieldConfig.class) $field.addClass(fieldConfig.class);
    if (fieldConfig.css) $field.css(fieldConfig.css);

    // FILL FIELD
    fillField(viewified, $field, fieldConfig, viewified.entry);

  });

  //
  //                              CSS CUSTOMIZATION

  if (viewified.options.cssContent) $container.css(viewified.options.cssContent);
  if (viewified.options.cssRules) {
    // remove any preexisting css rules added by a window of the same category
    if (viewified.options.category) $(".viewify_styling--"+ viewified.options.category).remove();
    // add css rules to head
    var style = document.createElement("style");
    if (viewified.options.category) style.classList = ["viewify_styling--"+ viewified.options.category];
    style.innerHTML = viewified.options.cssRules;
    document.head.appendChild(style);
  };

  //                              ¬
  //

};
