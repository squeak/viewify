# Usage

Load and use viewify library with `viewify(options)`.  

Or in more details:
```javascript
var entry = {
  _id: "abcdefghijklmnopqrstuvwxyz",
  something: "yo",
  color: "#F00",
  title: "yopchou",
  val: "ue",
};
viewify({
  entry: entry,
  title: entry.title,
  subtitle: entry.something,
  color: entry.color,
  fields: [
    { key: "_id", },
    { key: "val", },
    { display: function (entry, $fieldValue, $body) { return "Entry has "+ _.size(entry) +" keys." } },
  ],
});
```
