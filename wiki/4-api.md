# API

🛈 This API is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


## Options

```javascript
viewifyOptions = <{

  //
  //                              ENTRY

  !entry: <object> « the entry you want to display »,

  //
  //                              CATEGORY / ENTRY TYPE

  ?category: <string> «
    entry type or window category, this will be added as class to the viewify window (prefixed with "viewify-")
    specifying the window category is useful if you want to use the "cssRules" option, without category viewify will just add the css rules with each newly created window, while with the category, it will make it only once by category
  »,

  //
  //                              GENERAL

  ?title: <string> « the title to display in top bar »,
  ?subtitle: <string> « the subtitle to display in top bar »,
  ?icon: <string> « the icon to display in top bar »,
  ?image: <string> « an image instead of the icon, to display in top bar (if this is defined, and icon also, this will have priority) »,
  ?bodyColor: <string> « a color to give to the view window body »,
  ?titleBarColor: <string> « a color to give to the view window title bar »,

  //
  //                              FURTHER STYLING

  ?cssWindow: <object> « custom css to apply to the window »,
  ?cssContent: <object> « custom css to apply to the content »,
  ?cssRules: <string> «
    css text that will be added as given to the document, so you can easily and profoundly customize viewify display
    be very specific with the naming you use for classes and dom elements, because every css rule you put in here will affect the whole page
  »,
  ?postprocess: <function($field, fieldOptions)> « will be executed when field has been created »,
  ?viewifyOptionsMaker: <function(ø):<viewify·options>> « allow you to add any options that will be recalculated every time the view window is refreshed »,

  //
  //                              FIELDS

  ?fields: <viewify·field[]> « the list of fields to display in the body of the view page »,

  //
  //                              BUTTONS

  ?buttons: <uify.button·options[]> « a list of buttons to add in the menu bar »,
  ?buttonsContext <any> « context passed to titlebar buttons (will be the this of click callbacks functions for those buttons) »,

  //
  //                              EVENTS

  ?beforeClose: <function(ø){@this=viewified}> « executed when the window is to be closed, if this method returns `false`, the window will not be closed »,
  ?closed: <function(ø){@this=viewified}> « executed when the window has been closed »,

  //                              ¬
  //

}>
```

Types used in viewifyOptions:
```javascript
viewify·field = <{

  //
  //                              IF KEY

  ?deepKey: <deepKey> « key of the value to show, you may omit this if you set a display function »,
  ?label: <string> « the label to display (if this is not defined, will use deepKey as label) »,
  ?labelLayout: <"hidden"|"inline"|"stacked">@default="hidden" « if not hidden, displays "key: value" instead of only the value (e.g. "date: 1873-10-02") »,
  ?showEvenIfUndefined: <boolean> « If true, will not hide this field if the value is undefined. »,
  ?display: <function(
    entry <couchEntry> « current entry »,
    $fieldValue <yquerjObject> « the container where to put values in this $field »,
    $field <yquerjObject> « the global container of this field »,
    deepKey <string|number> « the deepKey of this value »,
    asyncDoneCallback <function(ø)> « if this display function is asynchronous, execute this callback when display has completed »,
  ){@this=viewified}:<"async"|undefined> «
    a custom way to display this key's value
    if this is function is defined, will not get value by key's name
    to add content to a field with this function append it to the passed $fieldValue element
    if this function is asynchronous return "async" here, to display a spinner,
    this spinner will be destroyed when asyncDoneCallback will be ran
  »,
  ?postprocess: <function(
    $field <yquerjObject> « the global container of this field »,
    fieldOptions <viewify·field> « the options passed to configure this field display »,
  ){@this=viewified}> « if this is defined, it will be used to postprocess this field dom element after creating it »,

  //
  //                              STYLES TO APPLY

  ?class: <string> « class to apply to this field »,
  ?css: <string> « css to apply to this field »,
  ?keyCss: <string> « css to apply to this field key »,

  //                              ¬
  //

}> « if no value is gotten and display is not a function, the field will be hidden »
```

## Return

The following is the type of the object returned by viewify:
```javascript
viewified = {
  isViewified: <true>,
  entry: <object>,
  options: <viewifyOptions>,
  windified: <windify·return>,
  tabs: <uify.tabs·return>,
  refresh: <function(newEntry<couchEntry>):<void>> « refresh display of the view window »,
}
```
