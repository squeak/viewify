var viewify = require("../../../scripts");
var uify = require("uify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "viewify",
    demo: function ($container) {

      uify.button({
        $container: $container,
        icomoon: "rocket",
        title: "show viewify window",
        inlineTitle: "bottom",
        css: { fontSize: "2em", },
        click: function () {
          var entry = $$.random.entry([
            {
              _id: "abcdefghijklmnopqrstuvwxyz",
              something: "yo",
              color: "#F00",
              title: "yopchou",
              val: "ue",
            },
            {
              _id: "0123456789",
              something: "something",
              color: "peachpuff",
              title: "HI",
              val: "value",
              another: "key",
            }
          ]);
          displayViewifyWindow(entry)
        },
      });

      function displayViewifyWindow (entry) {

        viewify({
          entry: entry,
          title: entry.title,
          subtitle: entry.something,
          color: entry.color,
          fields: [
            { deepKey: "_id", },
            { deepKey: "val", },
            { display: function (entry, $fieldValue, $body) { $fieldValue.div({ text: "Entry has "+ _.size(entry) +" keys." }); }, },
          ],
        });

      };

    },
  });

};
